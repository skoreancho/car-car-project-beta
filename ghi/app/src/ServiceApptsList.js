import React, { useState, useEffect } from 'react'

function ServiceApptsList() {
    const [appointments, setAppointments] = useState([]);

    const getData = async () => {
        const response = await fetch("http://localhost:8080/api/appointments/");

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const cancelAppointment = async (id) => {

        const deleteFunctionUrl = `http://localhost:8080/api/appointments/${id}/`
        const options = {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json"
            }

        }
        const response = await fetch(deleteFunctionUrl, options)
        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        }
    }

    const finishAppointment = async (id) => {

        const finishFunctionUrl = `http://localhost:8080/api/appointments/${id}/`
        const options = {
            method: 'PUT',
            body: JSON.stringify({ finished: true }),
            headers: { 'Content-Type': 'application/json' }

        }
        console.log(finishFunctionUrl)
        const response = await fetch(finishFunctionUrl, options)
        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        }
    }

    return (
        <>
        <h1>Appointments</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Reason</th>
                    <th>Technician</th>
                    <th>VIP</th>
                    <th>Finished/Cancel</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.technician ? appointment.technician.name : ''}</td>
                            <td>
                                <input type="checkbox" checked={appointment.vip} onChange={() => {}} />
                            </td>
                            <td>
                                <button type="button" className="btn btn-success"
                                    onClick={() => finishAppointment(appointment.id)}
                                >Finished</button>
                                <button type="button" className="btn btn-danger"
                                    onClick={() => cancelAppointment(appointment.id)}
                                >Cancel</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        <div>
            <a href='http://localhost:3000/appointments/new/'><button type='button'>New Appointment</button></a>
        </div>
        <a href='http://localhost:3000/appointments/history/'><button type='button'>Appointment History</button></a>
        </>
    );
}

export default ServiceApptsList;
