import React from 'react';

class TechnicianForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            employee_num: "",
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmployeeNumChange = this.handleEmployeeNumChange.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handleEmployeeNumChange(event) {
        const value = event.target.value;
        this.setState({ employee_num: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };

        const technicianUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            this.setState(
                {
                    name: "",
                    employee_num: "",
                }
            );
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new technician</h1>
                        <form onSubmit={this.handleSubmit} id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.name}
                                    onChange={this.handleNameChange}
                                    placeholder="Name"
                                    required
                                    type="text"
                                    name="name"
                                    id="name"
                                    className="form-control"
                                />
                                <label htmlFor="name"> Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.employee_num}
                                    onChange={this.handleEmployeeNumChange}
                                    placeholder="EmployeeNum"
                                    required
                                    type="text"
                                    name="employee_num"
                                    id="employee_num"
                                    className="form-control"
                                />
                                <label htmlFor="employee_num"> Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                    <div>
                        <a href='http://localhost:3000/technicians/'><button type='button'>Technicians</button></a>
                    </div>
                </div>
            </div>
        );
    }
}

export default TechnicianForm;
