import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadData() {
  const manufacturerResponse = await fetch('http://localhost:8100/api/manufacturers/');
  const automobileResponse = await fetch('http://localhost:8100/api/automobiles/');
  const saleResponse = await fetch('http://localhost:8090/api/sales/');
  if (manufacturerResponse.ok && automobileResponse.ok) {
    const manufacturerData = await manufacturerResponse.json();
    const automobileData = await automobileResponse.json()
    const saleData = await saleResponse.json()
    root.render(
      <React.StrictMode>
        <App manufacturers={manufacturerData.manufacturers} automobiles={automobileData.autos} sales={saleData.sales}/>
      </React.StrictMode>
    );
  } else {
    console.error(manufacturerResponse)
    console.error(automobileResponse)
    console.error(saleResponse)
  }
}

loadData();
