import React, { useState, useEffect } from 'react'

function TechniciansList() {
    const [technicians, setTechnicians] = useState([]);

    const getData = async () => {
        const response = await fetch("http://localhost:8080/api/technicians/");

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
        getData()
    }, [])


    return (
        <>
        <h1>Technicians</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Employee Number</th>
                </tr>
            </thead>
            <tbody>
                {technicians.map(technician => {
                    return (
                        <tr key={technician.id}>
                            <td>{technician.name}</td>
                            <td>{technician.employee_num}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        <div>
            <a href='http://localhost:3000/technicians/new/'><button type='button'>New Technician</button></a>
        </div>
        </>
    );
}

export default TechniciansList;
