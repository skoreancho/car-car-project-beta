import React from 'react';

class ModelForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            picture_url: "",
            manufacturer_id: "",
            manufacturers: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    }

    async componentDidMount() {
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(manufacturerUrl);

        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers });
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({ picture_url: value });
    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer_id: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.manufacturers;

        const modelsUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(modelsUrl, fetchConfig);
        if (response.ok) {
            this.setState(
                {
                    name: "",
                    picture_url: "",
                    manufacturer_id: "",
                }
            );
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new model</h1>
                        <form onSubmit={this.handleSubmit} id="create-model-form">
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.name}
                                    onChange={this.handleNameChange}
                                    placeholder="Name"
                                    required
                                    type="text"
                                    name="name"
                                    id="name"
                                    className="form-control"
                                />
                                <label htmlFor="name"> Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    value={this.state.picture_url}
                                    onChange={this.handlePictureUrlChange}
                                    placeholder="Picture URL"
                                    required
                                    type="url"
                                    name="picture_url"
                                    id="picture_url"
                                    className="form-control"
                                />
                                <label htmlFor="picture_url">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    value={this.state.manufacturer_id}
                                    onChange={this.handleManufacturerChange}
                                    required
                                    name="manufacturer"
                                    id="manufacturer"
                                    className="form-select"
                                >
                                    <option value="">Choose a Manufacturer</option>
                                    {this.state.manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                    <div>
                        <a href='http://localhost:3000/models/'><button type='button'>Models</button></a>
                    </div>
                </div>
            </div>
        );
    }
}

export default ModelForm;
