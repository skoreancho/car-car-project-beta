import React, {useEffect, useState} from 'react';

function SalespersonForm() {
    const [name, setName] = useState('');
    const [number, setNumber] = useState('');

    const handleNameChange = event => {
        const value = event.target.value;
        setName(value);
    }

    const handleNumberChange = event => {
        const value = event.target.value;
        setNumber(value);
    }

    const handleSubmit = async event => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.number = number;


        const url = "http://localhost:8090/api/salespersons/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setName('');
            setNumber('');
        }
    };

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a salesperson</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleNumberChange} value={number} placeholder="Number" required type="number" name="number" id="number" className="form-control" />
                <label htmlFor="number">Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default SalespersonForm;
