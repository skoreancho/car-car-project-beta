import React, {useState, useEffect} from 'react';

function SaleList() {

  const [sales, setSales] = useState([]);

  const saleList = async () => {
    const saleUrl = "http://localhost:8090/api/sales/";
    const saleResponse = await fetch(saleUrl);
    if (saleResponse.ok) {
      const saleData = await saleResponse.json();
      setSales(saleData.sales)
    }
  }

  useEffect(() => {
    saleList();
  }, []);

  return (
  <>
    <div className=" py-4 text-left">
      <h1>Sales</h1>
    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Salesperson</th>
          <th>Employee number</th>
          <th>Automobile VIN</th>
          <th>Customer</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {sales.map(sale => {
          return (
            <tr key={sale.id}>
              <td>{ sale.salesperson.name }</td>
              <td>{ sale.salesperson.number }</td>
              <td>{ sale.automobile.vin }</td>
              <td>{ sale.customer.name }</td>
              <td>{ sale.price }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  )
}

export default SaleList;
