import React, { useState, useEffect } from 'react'

function ServiceApptHistory() {
    const [history, setHistory] = useState([]);
    const [search, setSearch] = useState('');
    const [results, setResults] = useState([])

    const handleInputChange = (e) => {
        setSearch(e.target.value);
    };



    const getData = async () => {
        const response = await fetch("http://localhost:8080/api/history/");
        const data = await response.json()

        setHistory(data.history)
        setResults(data.history)

    }

    useEffect(() => {
        getData()
    }, [])



    return (
        <>
        <form>
            <input className="form-control" value={search} onChange={handleInputChange} placeholder="Search VIN Number"/>
            <button>Search VIN</button>
        </form>
        <h1>Service Appointment History</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Reason</th>
                    <th>Technician</th>
                </tr>
            </thead>
            <tbody>
                {history.map(hist => {
                    return (
                        <tr key={hist.id}>
                            <td>{hist.vin}</td>
                            <td>{hist.customer}</td>
                            <td>{hist.date}</td>
                            <td>{hist.time}</td>
                            <td>{hist.reason}</td>
                            <td>{hist.technician ? hist.technician.name : ''}</td>
                            <td></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    );
}

export default ServiceApptHistory;
