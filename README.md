# CarCar

Team:

* Person 1 - Kristen Lungstrum - Automobile Service
* Person 2 - Sean Cho - Auto Sales

## Run the Project

1. Clone this repo into your local device with the following command in your terminal: git clone <repo name>
2. Start Docker on your device
3. In your terminal, switch to the project directory
4. Run the following commands in order:
* docker volume create beta-data
* docker-compose build
* docker-compose up
5. Make sure that everything is running in your docker and visit http://localhost:3000/

## Design
* Project Diagram:
- ![Project Diagram](https://i.imgur.com/uPOpl62.png)
* Inventory APIs:
- ![Manufacturer APIs](https://i.imgur.com/os9ZR0Q.png)
- ![Vehicle APIs](https://i.imgur.com/f0LDN6u.png)
- ![Automobile APIs](https://i.imgur.com/2YK8drF.png)
* Service APIs:
- ![Service Appointments APIs](https://i.imgur.com/dufHiQ5.png)
- ![Technician APIs](https://i.imgur.com/jHNOzNx.png)
* Sales APIs:
- ![Salesperson APIs](https://i.imgur.com/s9bqZWE.png)
- ![Customer APIs](https://i.imgur.com/NLH1oCf.png)
- ![Sale APIs](https://i.imgur.com/ryNxlax.png)

## Inventory microservice
The inventory service serves as the database that we will be pulling from to create the VO objects used by the service and sales microservices. Inventory contains the Manufacturer, Vehicle, and Automobile Model. The Manufacturer Model provides the name for the vehicle Model which provides the specific vehicle model. The automobile Model contains a specific year, model, color, and vin for each instance.

## Service microservice

The Service Microservice contains three models
- Appointment
    This model allows the user to create a Service Appointment that includes the VIN number that is polled from AutomobileVO, the name of the customer, the date and time of the appointment, the reason for the appointment, the technician for the appointment that is pulled from the Technician model, and a way to track VIP status and completion of the appointment.
- Technician
    This model allows the user to create a technician using their name and employee number.
- AutomobileVO
    This value object polls from the Automobile model in the Inventory microservice to display the automobile's VIN number.

## Sales microservice

The sales microservice contains the AutomobileVO, Salesperson, Customer, and Sale Models as well as a poller. The poller polls the inventory microservice's database of automobiles and uses the polled data to create instances of the AutomobileVO model. The AutomobileVO model also slightly differs from the Automobile Model from the Inventory microservice in that there is a check for whether or not the automobile has been sold. This check is a boolean value with a default value of false. The Customer and Salesperson models are what we use to add new customers or salespeople from the front end. These newly created instances are then used to populate the dropdowns on the "Create a sale" form on the front end. The "Create a sale" form has the options to choose an existing automobile, customer, and salesperson from the sales microservice data along with a price number field. Once the form is completed and submitted, the specific automobileVO selected in the submitted form has its sold property changed from false to true, which ensures that it does not appear again in the dropdown of available vehicles for a new sale form.
