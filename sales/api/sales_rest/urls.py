from django.urls import path

from .views import api_list_salesperson, api_list_sale, api_list_customer, api_list_automobiles

urlpatterns = [
    path("salespersons/", api_list_salesperson, name="api_list_salesperson"),
    path("sales/", api_list_sale, name="api_list_sale"),
    path("customers/", api_list_customer, name="api_list_customer"),
    path("automobiles/", api_list_automobiles, name="api_list_automobiles")
]
