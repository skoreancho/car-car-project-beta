from django.urls import path
from .views import api_technician, api_technicians, api_appointments, api_appointment, api_appt_history

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/<int:pk>/", api_technician, name="api_technician"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:pk>", api_appointment, name="api_appointment"),
    path("history/<str:vin>",api_appt_history,name="api_appt_history"),
]
