from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_num = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})


class Appointment(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    reason = models.TextField(max_length=300)
    vip = models.BooleanField(default=True)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE,
        null=True,
    )
    finished = models.BooleanField(default=True)

    def __str__(self):
        return self.customer

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})
